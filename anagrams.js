// show anagrams of a word
document.getElementById("findButton").onclick = function() {
    groupedAnagrams = [];

    for(let word of words) {
         // puts the letters into a standard order, regardless of how they were originally arranged in the word
        // this whole function could even go outside of the for loop
        function alphabetize(i) {
            // returns whatever you throw in here; lowercase, split string into array of substrings, alphabetize, joins all elements of array into string and reutrns is 
            return i.toLowerCase().split("").sort().join("").trim();
        }

        // find what was typed in text field
        // could do above and on top (right at the top of onlcik function) or inside and on top (liek here)
        let typedText = document.getElementById("inputBox").value;    
        let orderedTypedText = alphabetize(typedText);
        if (orderedTypedText.length == word.length && orderedTypedText == alphabetize(word)) {
            // push is for arrays; if obj, it gets unfedined bx push isnt key; property
            // [].push() 
            groupedAnagrams.push(word);
        }
    }
    
    let span = document.createElement("span");
    let text = document.createTextNode(groupedAnagrams.join(", "));
    span.appendChild(text);
    let placeHere = document.getElementById("anagramsHere");
    placeHere.appendChild(span);
}


// show sets with five or more anagrams
document.getElementById("setsWithFivePlus").onclick = function() {
    let bigAnagramSets = {};
    
    function alphabetize(i) {
        // returns whatever you throw in here; lowercase, split string into array of substrings, alphabetize, joins all elements of array into string and reutrns is 
        return i.toLowerCase().split("").sort().join("").trim();
    }

    for(let word of words) {
        let orderedText = alphabetize(word);
        // if the object has an undefined orderedtext/aplhaword/is empty, put in the word; else, put in the word
        if (bigAnagramSets[orderedText] == undefined) {
            bigAnagramSets[orderedText] = [word]
        } else {
            bigAnagramSets[orderedText].push(word)
        }
    }

    // a thing inside the object set
    for(let i in bigAnagramSets) {
        let wordInput = bigAnagramSets[i];
        // if the group fo things inside the bAS is greater or less than 5, show it
        if(wordInput.length >= 5) {
            showAnswers(wordInput)
        }
    }

    // this is the function for above
    function showAnswers(wordInput) {
        let span = document.createElement("div");
        let text = document.createTextNode(wordInput.join(", "));
        span.appendChild(text);
        let placeHere = document.getElementById("bigSetsHere");
        placeHere.appendChild(span);
    }
}